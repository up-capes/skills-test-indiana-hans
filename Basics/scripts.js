var uNameCorrect = "hans"
var pWordCorrect = "1234"
var login = document.getElementById("login");
var confBG = document.getElementById("confirmationBG");
var confText = document.getElementById("confirmationText");

login.onclick = function() {loginFxn()};

function loginFxn() {
    if ((document.getElementById("uname").value == uNameCorrect) && (document.getElementById("pword").value == pWordCorrect)) {
        confBG.style.backgroundColor = "#8be8cbff";
        confText.innerHTML = "Successfully logged in!";
    }
    else{
        confBG.style.backgroundColor = "#9c7a97ff";
        confText.innerHTML = "Login unsuccessful.";
    }
}